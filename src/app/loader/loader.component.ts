import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoaderService } from '../services/loader/loader.service';
import { LoaderState } from '../shared/models/loader-state';

@Component({
  selector: 'app-loader',
  templateUrl: 'loader.component.html',
  styleUrls: ['loader.component.scss']
})
export class LoaderComponent implements OnInit {

  show = false;
  private subscription: Subscription;

  constructor(private loaderService: LoaderService) { }

  ngOnInit() {
    console.log("INITIALIZED")
    this.subscription = this.loaderService.loaderState
      .subscribe((state: LoaderState) => {
        console.log('state',state);
        this.show = state.show;
      });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    if (this.subscription) {
      
      this.subscription.unsubscribe();
    }
  }

}
