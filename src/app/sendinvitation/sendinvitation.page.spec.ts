import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SendinvitationPage } from './sendinvitation.page';

describe('SendinvitationPage', () => {
  let component: SendinvitationPage;
  let fixture: ComponentFixture<SendinvitationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendinvitationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SendinvitationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
