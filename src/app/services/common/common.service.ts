import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController, Platform, ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ApiurlsService } from 'src/app/cores/apiurls/apiurls.service';
import { LoaderService } from '../loader/loader.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  backButtonSubscription: any;
  public counter = 0;
  loadingVar: any;
  constructor(
    public loader: LoaderService,
    public http: HttpClient, public loadingController: LoadingController, public alertController: AlertController,
    public toastController: ToastController, public platform: Platform, public router: Router, public apiUrl: ApiurlsService) { }

  async presentAlert(header, msg) {
    const alert = await this.alertController.create({
      header: header,
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  handleHardwareBackButton() {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      console.log(this.router.url);
      if (this.router.url == '/app/tabs/tab1') {
        if (this.counter == 0) {
          this.counter++;
          this.presentToast();
          setTimeout(() => { this.counter = 0 }, 3000)
        } else {
          navigator['app'].exitApp();
        }
      }
    });
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Press again to exit',
      duration: 3000
    });
    toast.present();
  }

  async presentSecurityAlert(header, msg) {
    const alert = await this.alertController.create({
      header: header,
      message: msg,
      buttons: [{
        text: 'Okay',
        handler: () => {
          console.log('Confirm Okay');
          localStorage.clear();
          this.router.navigate(['/login']);
        }
      }]
    });
    await alert.present();
  }

  async presentLoading() {
    // this.loadingVar = await this.loadingController.create({
    //   message: 'Working our magic....',
    //   cssClass: 'custom_loader',
    //   spinner: null,
    //   showBackdrop: false,
    //   //translucent:true
    // });
    // await this.loadingVar.present();

    await this.loader.show()
  }

  dismissLoading() {
    this.loader.hide()
    // console.log(this.loadingVar)
    // this.loadingVar.dismiss();
  }

  async getUser_detail() {
    new Promise((resolve, reject) => {
      this.http.get(this.apiUrl.getUserDetail).subscribe((res: any) => {
        console.log(res)
        if (res.success == true) {
          return res.data;
        }
      })
    })
  }

  priceCalculation(price,off){
   // console.log(price,off)
    let totalprice = parseFloat(price) - (parseFloat(price) * parseFloat(off))/100;
    //console.log(totalprice)
    return totalprice;
  }
}
