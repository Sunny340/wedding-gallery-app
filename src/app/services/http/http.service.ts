import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiurlsService } from 'src/app/cores/apiurls/apiurls.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(public http: HttpClient, public apiUrl: ApiurlsService) { }

  get(url) {
    /********* common function to http get ***********/
    return this.http.get(url);
  }

  post(url, postdata) {
    /********* Common function to http post ********/
    return this.http.post(url, postdata);
  }

  delete(url, postdata) {
    /********* Common function to http delete ********/
    let httpParams = new HttpParams().set('addressId', postdata.addressId);
    let options = { params: httpParams };
    return this.http.delete(url, options);
  }
  
  put(url, postdata) {
    /********* Common function to http put ********/
    return this.http.put(url, postdata);
  }

}
