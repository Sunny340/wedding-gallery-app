import { LoaderService } from '../loader/loader.service'; 
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  private requests: HttpRequest<any>[] = [];

  constructor(private loader: LoaderService) { }
  // intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  //   var token = localStorage.getItem('userdata') ? JSON.parse(localStorage.getItem('userdata')).api_token : '';
  //   const authReq = request.clone({
  //     headers: new HttpHeaders({
  //       'accessToken': 'base64:mcP33ANGzmG5F7LmzUoXICw/Fkje0yCuiGJkw+xCp+Q=',
  //       'Authorization': 'Bearer ' + token
  //     })
  //   });
  //   return next.handle(authReq);
  // }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    var token = localStorage.getItem('userdata') ? JSON.parse(localStorage.getItem('userdata')).api_token : '';
    let req = request.clone({
      headers: new HttpHeaders({
        'accessToken': 'base64:mcP33ANGzmG5F7LmzUoXICw/Fkje0yCuiGJkw+xCp+Q=',
        'Authorization': 'Bearer ' + token
      })
    });

    const index = this.loader.backGroundUrls.indexOf(req.url.toString());

    if (index < 0) {
      this.requests.push(req);
      this.loader.show();
    } else {
      this.loader.backGroundUrls.splice(index, 1);
    }

    return next.handle(req).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        this.removeRequest(req);
      }
    }, (err: any) => {
      this.removeRequest(req);
    }));
  }

  removeRequest(req: HttpRequest<any>) {
    const i = this.requests.indexOf(req);
    this.requests.splice(i, 1);
    if (this.requests.length === 0) {
      this.loader.hide();
    }
  }
}
