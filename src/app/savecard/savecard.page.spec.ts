import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SavecardPage } from './savecard.page';

describe('SavecardPage', () => {
  let component: SavecardPage;
  let fixture: ComponentFixture<SavecardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavecardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SavecardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
