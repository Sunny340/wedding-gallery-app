import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SavecardPageRoutingModule } from './savecard-routing.module';

import { SavecardPage } from './savecard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SavecardPageRoutingModule
  ],
  declarations: [SavecardPage]
})
export class SavecardPageModule {}
