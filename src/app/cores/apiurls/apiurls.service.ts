import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiurlsService {
  loginApi:string = environment.baseUrl + '/login';
  verifyOtp:string = environment.baseUrl + '/verifyOtp';
  resendOtp:string = environment.baseUrl + '/reSendOtp';
  getOffersListApi:string = environment.baseUrl + '/offerList';
  parentCategoryByLocaion:string = environment.baseUrl + '/parentCategoryByLocation';
  categoryByParentIdApi:string =  environment.baseUrl + '/categoryByParentIdWhereLocation';
  catById:string = environment.baseUrl + '/catById';
  serviceByKeyword:string = environment.baseUrl + '/serviceByKeyword';
  serviceByCategoryIdApi:string = environment.baseUrl + '/serviceByCatId';
  addAddressApi:string = environment.baseUrl + '/addAddress';
  addressListApi:string = environment.baseUrl + '/addressList';
  configDataApi:string = environment.baseUrl + '/configdata';
  getTimeSlotApi:string = environment.baseUrl + '/getDateTimeSlot';
  makeBookingApi:string = environment.baseUrl + '/bookNow';
  getUserDetail:string = environment.baseUrl + '/userDetails';
  myBookingApi:string = environment.baseUrl + '/myBooking';
  cancelBookingApi:string = environment.baseUrl + '/bookingCancel';
  deleteAddressApi:string = environment.baseUrl + '/deleteAddress';
  editAddressApi:string = environment.baseUrl + '/editAddress';
  getCancellationReason:string = environment.baseUrl + '/cancellationResaon';
  removeHistoryApi:string = environment.baseUrl + '/removeBooking'; 
  faqApi:string = environment.baseUrl + '/faq';
  getBookingDetailApi:string = environment.baseUrl + '/bookingDetails';
  savePaymentApi:string = environment.baseUrl + '/savePayment';
  bookingRescheduleApi:string = environment.baseUrl + '/bookingReschedule';
  aboutusApi:string = environment.baseUrl + '/getPage?slug=about';
  bookingDetailApi:string = environment.baseUrl + '/bookingDetails';
  contactusApi:string = environment.baseUrl + '/contactUs';
  walletApi:String = environment.baseUrl + '/loadWallet';
  servicesList:string = environment.baseUrl + '/serviceWhereCatIds';
  paymentWithWalletApi:string = environment.baseUrl + '/payByWallet';
  searchbykeywordApi:string = environment.baseUrl + '/serviceByKeyword';
  editprofileApi:string = environment.baseUrl + '/editProfile';
  constructor() { }
}
