import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SavecardPage } from './savecard.page';

const routes: Routes = [
  {
    path: '',
    component: SavecardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SavecardPageRoutingModule {}
