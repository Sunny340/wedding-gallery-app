import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators,AbstractControl  } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common/common.service';
import { HttpService } from 'src/app/services/http/http.service';
import { ApiurlsService } from 'src/app/cores/apiurls/apiurls.service';
import { Platform } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginform:any;
  subscription:any;
  formStatus:any = { 
    submitted : false,
    verified : false
  }
  constructor(private formbuilder : FormBuilder, 
              private platform : Platform,
              private http : HttpService,
              private router : Router,
              private apiurl: ApiurlsService,
              private commonService : CommonService
  ) {

    var user = localStorage.getItem('userdata');
    if(user){
      this.router.navigate(['app/tabs/tab1'])
    }
   }


  ionViewDidEnter(){
    this.subscription = this.platform.backButton.subscribe(()=>{
          navigator['app'].exitApp();
      });
  }

  ionViewWillLeave(){
      this.subscription.unsubscribe();
  }

  ngOnInit() {
    this.loginform = this.formbuilder.group({
      phone_email : ['', [Validators.required,this.formatValidator.bind(this)]]
    })
  }


  formatValidator(control: AbstractControl) {
    let pattern =/^(?:\d{10}|\w+@\w+\.\w{2,3})$/
    var str = control.value; 
    var res = str.match(pattern);
    if(res == null){
      return { format: true }
    }
    return null;
  }




  login(){
    this.commonService.presentLoading();
    var postdata = {
      phone: '+91'+this.loginform.value.phone_email,
      iso_code_2: 'IN'
    }
    this.http.post(this.apiurl.loginApi, postdata).subscribe((res: any) => {
      console.log(res);
      this.commonService.dismissLoading();
      if (res.success == true) {
        res.data.verified = false;
        localStorage.setItem('phone', '+91'+this.loginform.value.phone_email);
        localStorage.setItem('userdata', JSON.stringify(res.data));
        this.router.navigate(['/otp']);
      }
    }, error => {
      this.commonService.dismissLoading();
      this.commonService.presentAlert('Network error', 'Server connection failed');
    });

  }



  fb_login(){

  }

  google_login(){

  }
}