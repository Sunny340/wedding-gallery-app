import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { CommonService } from 'src/app/services/common/common.service';
import { HttpService } from 'src/app/services/http/http.service';
import { ApiurlsService } from 'src/app/cores/apiurls/apiurls.service';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.page.html',
  styleUrls: ['./otp.page.scss'],
})
export class OtpPage implements OnInit {
  phoneNumber: any;
  id: string;
  timer: number = 60;
  source: any;
  otpform:any;
  constructor(public formbuilder: FormBuilder, public router: Router, public http: HttpService, public loadingController: LoadingController, 
    public commonService: CommonService,public apiurl:ApiurlsService) {
    this.phoneNumber = localStorage.getItem('phone');
    this.id = localStorage.getItem('id');
    this.getCounter();
  }

  ngOnInit() {
    this.otpform = this.formbuilder.group({
      otp: ['', [Validators.required, Validators.minLength(4)]],
    })
  }

  verify(){
    this.commonService.presentLoading();
      var postdata = {
        otp: this.otpform.value.otp
      }
      this.http.post(this.apiurl.verifyOtp, postdata).subscribe((res: any) => {
       // console.log(res);
        this.commonService.dismissLoading();
        if (res.success == true) {
          clearInterval(this.source);
          res.data.verified = true;
          localStorage.setItem('userdata', JSON.stringify(res.data));
          this.router.navigate(['app/tabs/tab1']);
        } else {
          this.otpform.reset();
          this.commonService.presentAlert('OTP', res.message);
        }
      }, error => {
        this.commonService.dismissLoading();
        if (error.status == 401) {
          return this.commonService.presentSecurityAlert('Security alert', 'You have been logged out for security purpose. Please login again.')
        } else {
          this.commonService.presentAlert('Warning', 'Server not responding');
        }
      });
  }

  async resendOtp() {
    /************* Function to resend the otp *************/
    this.commonService.presentLoading();
    var postdata = {
      phone: localStorage.getItem('phone')
    }
    this.http.post(this.apiurl.resendOtp, postdata).subscribe((res: any) => {
      console.log(res);
      this.commonService.dismissLoading();
      if (res.success == true) {
        localStorage.setItem('userdata', JSON.stringify(res.data));
        this.timer = 60;
        this.getCounter();
        this.otpform.reset();
      }
    }, error => {
      this.commonService.dismissLoading();
      if (error.status == 401) {
        return this.commonService.presentSecurityAlert('Security alert', 'You have been logged out for security purpose. Please login again.')
      } else {
        this.commonService.presentAlert('Warning', 'Server not responding');
      }
    });
  }

  getCounter() {
    this.source = setInterval(() => {
      this.timer = this.timer - 1;
      if (this.timer < 1) {
        clearInterval(this.source);
      }
    }, 1000)
  }

  onKeyUp(event: any) {
    let newValue = event.target.value;
    let regExp = new RegExp('^[0-9]+$');
    if (! regExp.test(newValue)) {
      event.target.value = newValue.slice(0, -1);
    }
  }
}
