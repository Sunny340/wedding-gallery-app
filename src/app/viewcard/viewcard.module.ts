import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewcardPageRoutingModule } from './viewcard-routing.module';

import { ViewcardPage } from './viewcard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewcardPageRoutingModule
  ],
  declarations: [ViewcardPage]
})
export class ViewcardPageModule {}
