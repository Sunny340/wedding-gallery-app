import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewcardPage } from './viewcard.page';

describe('ViewcardPage', () => {
  let component: ViewcardPage;
  let fixture: ComponentFixture<ViewcardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewcardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewcardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
