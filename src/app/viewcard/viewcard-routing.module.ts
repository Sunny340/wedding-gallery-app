import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewcardPage } from './viewcard.page';

const routes: Routes = [
  {
    path: '',
    component: ViewcardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewcardPageRoutingModule {}
