import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./components/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'app',
    loadChildren: () => import('./components/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./components/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'otp',
    loadChildren: () => import('./components/otp/otp.module').then( m => m.OtpPageModule)
  },
  {
    path: 'invitation',
    loadChildren: () => import('./components/invitation/invitation.module').then( m => m.InvitationPageModule)
  },
  {
    path: 'savecard',
    loadChildren: () => import('./components/savecard/savecard.module').then( m => m.SavecardPageModule)
  },
  {
    path: 'sendinvitation',
    loadChildren: () => import('./components/sendinvitation/sendinvitation.module').then( m => m.SendinvitationPageModule)
  },
  {
    path: 'viewcard',
    loadChildren: () => import('./components/viewcard/viewcard.module').then( m => m.ViewcardPageModule)
  },
  {
    path: 'viewgallery',
    loadChildren: () => import('./components/viewgallery/viewgallery.module').then( m => m.ViewgalleryPageModule)
  }  
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
